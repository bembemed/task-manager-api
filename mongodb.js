// CRUD 

const {MongoClient,ObjectID} = require('mongodb')
// const mongoClient = mongodb.MongoClient

const connectionURL = 'mongodb://127.0.0.1:27017'
const database = 'taskmanager'


MongoClient.connect(connectionURL, {useNewUrlParser: true},(error,client)=>{
    if(error){
        return console.log('unable to connect to databaseURL');
    }
    const db = client.db(database)
    // db.collection('users').insertOne({
    //     name: 'sami',
    //     age: 27
    // },(err , res)=>{
    //     if(err){
    //         return console.log('unable to insert');
    //     }
    //     console.log(res.ops);
    // })

    // db.collection('users').insertMany([
    //     {
    //         name: 'khalihine',
    //         age: 27
    //     },
    //     {
    //         name: 'saa',
    //         age: 27
    //     }
    // ],(err , res)=>{
    //     if(err){
    //         return console.log('unable to insert');
    //     }
    //     console.log(res.ops);
    // })

    // db.collection('users').findOne({_id: new ObjectID("6554eb19d471183018bdc7a4")},(err,user)=>{
    //     if(err) {
    //         return console.log('unable to fecth');
    //     }
    //     console.log(user);
    // })

    // db.collection('users').find({age:27}).toArray((err, user)=>{
    //     console.log(user);
    // })
    // db.collection('users').find({age:27}).count((err, user)=>{
    //     console.log(user);
    // })


    db.collection('users').updateOne({
        _id : new ObjectID('6554eb19d471183018bdc7a4')
    },{
        $inc:{
            age:1
        }
    }).then((result)=>{
        console.log(result);
    }).catch((error)=>{
        console.log(error);
    })

    db.collection('users').updateMany({
        age: 27
    },{
        $inc:{
            age:1
        }
    }).then((result)=>{
        console.log(result.modifiedCount);
    }).catch((error)=>{
        console.log(error);
    })

    // db.collection('users').deleteMany({
    //     age: 27
    // }).then((result) => {
    //     console.log(result)
    // }).catch((error) => {
    //     console.log(error)
    // })

    db.collection('tasks').deleteOne({
        description: "Clean the house"
    }).then((result) => {
        console.log(result)
    }).catch((error) => {
        console.log(error)
    })


})