const express = require('express')
const User = require('../models/user')
const auth = require('../middleware/auth')
const multer = require('multer')
const router = express.Router()

router.post('/users',async (req,res)=>{
    const user = new User(req.body)

    try{
        await user.save()
        const token = await user.generateAuthToken()
        
        res.status(201).send({user,token})
    } catch(e){
        res.status(400).send(e)
    }
})
router.post('/users/login',async (req, res)=>{
    try {
        const user = await User.findByCredentials(req.body.email, req.body.password)
        const token = await user.generateAuthToken()
        res.send({user,token})  
    } catch (e) {
        res.status(400).send(e.toString())
    }
})
router.post('/users/logout',auth,async (req,res)=>{
    try{
        req.user.tokens = req.user.tokens.filter((token)=>{
            return token.token !==req.token 
        })
        await req.user.save()
        res.send()
    }catch(e){
        res.status(500).send()
    }
})
router.post('/users/logoutAll', auth, async (req, res) => {
    try {
        req.user.tokens = []
        await req.user.save()
        res.send()
    } catch (e) {
        res.status(500).send()
    }
})
router.get('/users',auth,async(req,res)=>{

    try{
        const users = await User.find({})
        res.send(users)
    }catch(e){
        res.status(500).send(e)
    }
})

router.get('/users/me',auth,async(req,res)=>{

    res.send(req.user)
})
router.get('/users/:id',auth,async(req,res)=>{

    const _id = req.params.id 
    try{
        const user = await User.findById(_id)  

        if(!user){
            return res.status(404).send()
        }
        res.status(200).send(user)
        
    }catch(e){
        res.status(400).send(e)
    }
})
router.patch("/users/me",auth, async (req,res)=>{
    const updates = Object.keys(req.body)
    const allowdUpdates = ['name',"email","password","age"]
    const isValidOperation = updates.every( (update)=>{
        return allowdUpdates.includes(update)
    })

    if(!isValidOperation){
        return res.status(400).send({error: 'Invalid updates!'})
    }

    try {
        // const user = await User.findByIdAndUpdate(req.params.id,req.body,{new: true, runValidators:true})
        // const user = await User.findById(req.params.id)
        updates.forEach((update)=> req.user[update] = req.body[update])
        await user.save()

        if(!user){
            return res.status(404).send() 
        }
        res.send(user)
    }catch(e){
        res.status(500).send()
    }
})
router.delete("/users/me", auth,async (req,res)=>{
    try {

        // const user = await User.findByIdAndDelete(req.user.id)
        // if(!user){
        //     return res.status(400).send()
        // }
        await req.user.remove()
        res.send(req.user)  
    }catch(e){
        res.status(500).send(e.toString())
    }
})

const upload = multer({
    dest: 'avatars',   
    limits:{
        fileSize:1000000
    },
    fileFilter(req,file,cb){
        if(!file.originalname.match(/\.(jpg|jpeg|png)$/)){
            return cb(new Error('please upload an image'))
        }

        cb(undefined,true)
    }
})

router.post('/users/me/avatar',auth ,upload.single('avatar'),async (req,res)=>{
    const buffer = await sharp(req.file.buffer).resize({ width: 250, height: 250 }).png().toBuffer()
    req.user.avatar = buffer 
    await req.user.save() 
    res.send(req.user)
},(err,req,res,next)=>{
   res.status(400).send({error:err.message})
})

router.delete('/users/me/avatar',auth ,async (req,res)=>{
    req.user.avatar = undefined
    await req.user.save() 
    res.send()
},(err,req,res,next)=>{
   res.status(400).send({error:err.message})
})

router.get('/users/:id/avatar', async (req, res) => {
    try {
        const user = await User.findById(req.params.id)

        if (!user || !user.avatar) {
            throw new Error()
        }

        res.set('Content-Type', 'image/jpg')
        res.send(user.avatar)
    } catch (e) {
        res.status(404).send()
    }
})

module.exports = router