const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const Task = require('./tasks')
const userSchema = new mongoose.Schema({
    name: {
        type: String, 
        required: true,
        trim: true    
    },
    avatar:{
        type: Buffer
    },
    email: {
        type: String,
        required: true,
        unique: true,
        trim: true,
        lowercase: true,
        validate(value){
            if(!validator.isEmail(value)){
                throw new Error('email is invalid')
            }
        }
    },
    password:{
        type: String,
        required: true,
        minlength: 7,
        trin: true,
        validate(value){
            if(value.toLowerCase().includes('password')){
                throw new Error('password cannot containe passwrd')
            }
        }
    },
    age:{
        type: Number,
        default: 0,
        validate(value){
            if(value< 0){
                throw new Error('age must be a positive')
            }
        }
    },
    tokens:[
        {
            token:{
                type: String,
                required: true
            }
        }
    ],
},{
    timestamps: true
})

userSchema.virtual('tasks',{
    ref: 'Task',
    localField: '_id',
    foreignField: 'owner'
})

userSchema.statics.findByCredentials = async (email, password)=>{
    const user = await User.findOne({email})

    console.log(user);
    if(!user){
        throw new Error('unable to login ')
    }

    const isMatch = await bcrypt.compare(password, user.password)

    if(!isMatch){
        throw new Error('password is wrong')
    }
    console.log(user)
    return user;
}

userSchema.methods.toJSON = function(){
    const user = this
    const userObject = user.toObject()

    delete userObject.password
    delete userObject.tokens 
     return userObject
}

//generate token 
userSchema.methods.generateAuthToken = async function(){
    const user = this
    const token = jwt.sign({_id: user._id.toString()}, "thisiswebnewcourse")

    user.tokens = user.tokens.concat({token})
    await user.save()  

    return token
}
//hash the credentials 
userSchema.pre('save', async function(next){
    const user = this  

    if(user.isModified('password')){
        user.password = await bcrypt.hash(user.password,8)
    }
    next()
})

//delete user task where user delete
userSchema.pre('remove',async function(next){
    const usr = this  
    await Task.deleteMany({owner: usr._id})

    next()
})
const User = mongoose.model('User',userSchema)

module.exports = User