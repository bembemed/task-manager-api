const jwt = require('jsonwebtoken')
const User = require('../models/user')

const auth = async (req,res,next)=>{
    try{
        const token = req.header('Authorization').replace('Bearer ','')
        
        const decoded = jwt.verify(token, 'thisiswebnewcourse')
        const user = await User.findOne({_id:decoded._id, "tokens.token": token})

        if(!user){
            throw new Error('error')
        }

        req.user = user     
        next()
    }catch(e){
        res.status(401).send({e:e,error: 'please AuthenticatorResponse.'})
    }
}

module.exports = auth